package pojo;

import java.util.Objects;

public class HomePageObj {
    private String login;
    private String pass;
    private String email;
    private String usernameLabelText;
    private String passLabelText;
    private String emailLabelText;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsernameLabelText() {
        return usernameLabelText;
    }

    public void setUsernameLabelText(String usernameLabelText) {
        this.usernameLabelText = usernameLabelText;
    }

    public String getPassLabelText() {
        return passLabelText;
    }

    public void setPassLabelText(String passLabelText) {
        this.passLabelText = passLabelText;
    }

    public String getEmailLabelText() {
        return emailLabelText;
    }

    public void setEmailLabelText(String emailLabelText) {
        this.emailLabelText = emailLabelText;
    }

}
