package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static conf.DriverUtils.getDriver;

public class Page {

    public WebElement find(String xpath){
        return getDriver().findElement(By.xpath(xpath));
    }
}
