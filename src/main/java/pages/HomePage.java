package pages;

public class HomePage extends Page{
    private static final String LOGIN_FIELD = "//input[@id='user[login]']";
    private static final String PASSWORD_FIELD = "//input[@id='user[password]']";
    private static final String EMAIL_FIELD = "//input[@id='user[email]']";
    private static final String USERNAME_LABEL = "//label[@for='user[login]']";
    private static final String PASS_LABEL = "//label[@for='user[password]']";
    private static final String EMAIL_LABEL = "//label[@for='user[email]']";

    public void setLogin(String login){
        find(LOGIN_FIELD).sendKeys(login);
    }

    public void setPass(String pass){
        find(PASSWORD_FIELD).sendKeys(pass);
    }
    public void setEmail(String email){
        find(EMAIL_FIELD).sendKeys(email);
    }

    public String getUsernameLabelText(){
        return find(USERNAME_LABEL).getText();
    }

    public String getPassLabelText(){
        return find(PASS_LABEL).getText();
    }

    public String getEmailLabelText(){
        return find(EMAIL_LABEL).getText();
    }
}
