package conf;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static java.util.Objects.isNull;

public class DriverUtils {
   static {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\src\\main\\resources\\chromedriver.exe");

    }

    private static WebDriver driver;

    public static WebDriver getDriver() {
        if (isNull(driver)) {
            driver = new ChromeDriver();
        }
        return driver;
    }


}
