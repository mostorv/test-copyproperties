import org.testng.annotations.Test;
import pages.HomePage;
import pojo.HomePageObj;

import java.lang.reflect.InvocationTargetException;

import static conf.DriverUtils.getDriver;
import static org.apache.commons.beanutils.BeanUtils.copyProperties;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

public class GitHubTest {

    @Test
    public void setDataToPage() throws InvocationTargetException, IllegalAccessException {
        HomePageObj data = new HomePageObj();
        data.setLogin("dsff");
        data.setEmail("email");
        data.setPass("my awesome pass");

        getDriver().get("https://github.com/");

        copyProperties(new HomePage(), data);

    }

    @Test
    public void getDataFromPage() throws InvocationTargetException, IllegalAccessException {
        HomePageObj expected = new HomePageObj();
        expected.setEmailLabelText("Email");
        expected.setPassLabelText("Password");
        expected.setUsernameLabelText("Username");

        getDriver().get("https://github.com/");


        HomePageObj actual = new HomePageObj();

        copyProperties(actual, new HomePage());

        assertReflectionEquals(expected, actual);
    }
}
